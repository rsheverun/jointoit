<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Role;
use App\User;
use AuthenticatesUsers;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
   
    public function __construct()
    {
        $this->middleware('auth');
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $role = Role::find(Auth::user()->role_id)->role;
        
        return view('dashboardContent',['role'=>$role]);
       
    }

    public function charts(){
       $role = Role::find(Auth::user()->role_id)->role;
       
        if($role == 'superAdmin' or $role == 'admin')
        return view('charts',['role'=>$role]);
       else abort(404);
        
    }
    public function tables(){
         $role = Role::find(Auth::user()->role_id)->role;
       if($role == 'manager' or $role == 'superAdmin'){
            return view('tables',['role'=>$role]);
       }else abort(404);
        
    }
    public function navbar(){
         $role = Role::find(Auth::user()->role_id)->role;
       if($role == 'admin' or $role == 'user' or $role == 'superAdmin'){
            return view('navbar',['role'=>$role]);
        }else abort(404);
    }
     public function cards(){

         $role = Role::find(Auth::user()->role_id)->role;
        if($role == 'admin' or $role == 'user' or $role == 'superAdmin'){
             return view('cards',['role'=>$role]);
        }else abort(404);
    }
    public function map(){

         $role = Role::find(Auth::user()->role_id)->role;
        if ($role == 'superAdmin') {
            return view('map',['role'=>$role]);
        }else abort(404);
    }
}
