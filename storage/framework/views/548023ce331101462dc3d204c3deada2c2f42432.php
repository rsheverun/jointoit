<?php $__env->startSection('content'); ?>
<ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Charts</li>
      </ol>
      <?php echo $__env->make('components.areaChart', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
     <div class="row">
     	<div class="col-lg-8">
     		<?php echo $__env->make('components.barChartCard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
     	</div>
     	<div class="col-lg-4">
     		<?php echo $__env->make('components.pieChardCard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
     	</div>
     </div>
	
	

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.tplAdmin', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>