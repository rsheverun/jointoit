<?php $__env->startSection('content'); ?>

      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">My Dashboard</li>
      </ol>
      <!-- Icon Cards-->
    <?php if($role == 'superAdmin' or $role == 'admin'): ?>
    <?php echo $__env->make('components.cards', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('components.areaChart', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
     <div class="row">
        <div class="col-lg-8">
     	
     		<?php echo $__env->make('components.barChartCard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    		<?php echo $__env->make('components.socialFeed', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	</div>
        <div class="col-lg-4">
        	<?php echo $__env->make('components.pieChardCard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
   		
    <?php echo $__env->make('components.notification', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
     <!-- Example DataTables Card-->
  <?php else: ?> 
  <?php endif; ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.tplAdmin', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>