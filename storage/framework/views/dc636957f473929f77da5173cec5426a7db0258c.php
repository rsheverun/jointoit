<?php $__env->startSection('content'); ?>
	<div class="container">
    <div class="card card-login mx-auto mt-5">
      <div class="card-header">Reset Password</div>
      <div class="card-body">
      	<?php if(session('status')): ?>
                        <div class="alert alert-success">
                            <?php echo e(session('status')); ?>

                        </div>
                    <?php endif; ?>
        <div class="text-center mt-4 mb-5">
          <h4>Forgot your password?</h4>
          <p>Enter your email address and we will send you instructions on how to reset your password.</p>
        </div>
        <form  method="POST" action="<?php echo e(route('password.email')); ?>">
        	 <?php echo e(csrf_field()); ?>

          <div class="form-group">
            <input class="form-control" id="exampleInputEmail1" type="email" aria-describedby="emailHelp" placeholder="Enter email address"  name="email" value="<?php echo e(old('email')); ?>" required>
            <?php if($errors->has('email')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('email')); ?></strong>
                                    </span>
                                <?php endif; ?>
          </div>
          <button type="submit" class="btn btn-primary btn-block" href="login.html">Reset Password</button>
        </form>
        <div class="text-center">
          <a class="d-block small mt-3" href="<?php echo e(route('register')); ?>">Register an Account</a>
          <a class="d-block small" href="<?php echo e(route('login')); ?>">Login Page</a>
        </div>
      </div>
    </div>
  </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.tplLoginForm', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>