<?php $__env->startSection('content'); ?>
<div id="map"></div>
    <script>
    	var markersData = [
	{
		lat: 50.50764258,     // Широта
		lng: 30.6777579,    // Долгота
		name: "name 1", 
		address:"Троещина"   
	},
	{
		lat: 50.36183325,
		lng: 30.43468539,
		name: "name 2",
		address:"address 2"
	},
	{
		lat: 50.50005106,
		lng: 30.79586093,
		name: "name 3",
		address:"address 3"
	}
];
    
var map, infoWindow;
function initMap() {
    var centerLatLng = new google.maps.LatLng(50.43361691, 30.52106539);
    var mapOptions = {
        center: centerLatLng,
        zoom: 9
    };
    map = new google.maps.Map(document.getElementById("map"), mapOptions);
    infoWindow = new google.maps.InfoWindow();
    google.maps.event.addListener(map, "click", function() {
        infoWindow.close();
    });
    
    for (var i = 0; i < markersData.length; i++){
        var latLng = new google.maps.LatLng(markersData[i].lat, markersData[i].lng);
        var name = markersData[i].name;
        var address = markersData[i].address;
        // Добавляем маркер с информационным окном
        addMarker(latLng, name, address);
    }
}
google.maps.event.addDomListener(window, "load", initMap);

function addMarker(latLng, name, address) {
    var marker = new google.maps.Marker({
        position: latLng,
        map: map,
        title: name
    });
    
    google.maps.event.addListener(marker, "click", function() {
       
        var contentString = '<div class="infowindow">' +
                                '<h3>' + name + '</h3>' +
                                '<p>' + address + '</p>' +
                            '</div>';
       
        infoWindow.setContent(contentString);
        
        infoWindow.open(map, marker);
    });
}
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA3yxgXKUJ-4TQOjn-PAhiOmwRawMwYmZQ&callback=initMap">
    </script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.tplAdmin', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>