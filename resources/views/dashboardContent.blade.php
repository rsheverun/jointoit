@extends('layouts.tplAdmin')
@section('content')

      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">My Dashboard</li>
      </ol>
      <!-- Icon Cards-->
    @if($role == 'superAdmin' or $role == 'admin')
    @include('components.cards')
    @include('components.areaChart')
     <div class="row">
        <div class="col-lg-8">
     	
     		@include('components.barChartCard')
    		@include('components.socialFeed')
	</div>
        <div class="col-lg-4">
        	@include('components.pieChardCard')
   		
    @include('components.notification')
     <!-- Example DataTables Card-->
  @else 
  @endif
@endsection