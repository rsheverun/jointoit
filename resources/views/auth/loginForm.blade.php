@extends('layouts/tplLoginForm')
@section('content')

	 <div class="container">
	    <div class="card card-login mx-auto mt-5">
	      <div class="card-header">Login</div>
	      <div class="card-body">
	        <form method="POST" action="{{ route('login') }}">
	        	{{ csrf_field() }}
	          <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
	            <label for="exampleInputEmail1">Email address</label>
	            <input class="form-control" id="exampleInputEmail1" type="email" aria-describedby="emailHelp" placeholder="Enter email" name="email" value="{{ old('email') }}" required>
	             @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
	          </div>
	          <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
	            <label for="exampleInputPassword1">Password</label>
	            <input class="form-control" id="exampleInputPassword1" type="password" placeholder="Password" name="password" required>

	             @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
	          </div>
	          <div class="form-group">
	            <div class="form-check">
	              <label class="form-check-label">
	                <input class="form-check-input" type="checkbox"> Remember Password</label>
	            </div>
	          </div>

	          <button type="submit" class="btn btn-primary btn-block">Login</button>
	          <div class="text-center">
		          <a class="d-block small mt-3" href="{{ route('register') }}">Register an Account</a>
		          <a class="d-block small" href="{{ route('password.request') }}">Forgot Password?</a>
	        </div>
	        </form>
	        
	      </div>
	    </div>
  </div>
@endsection