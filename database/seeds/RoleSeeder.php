<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
          DB::table('roles')->insert(['role'=>'superAdmin']);
          DB::table('roles')->insert(['role'=>'admin']);
          DB::table('roles')->insert(['role'=>'manager']);
          DB::table('roles')->insert(['role'=>'user']);

    }
}
