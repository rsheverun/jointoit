-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Июл 19 2018 г., 14:16
-- Версия сервера: 5.7.19
-- Версия PHP: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `test`
--

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_07_18_231201_create_table_role', 2),
(4, '2018_07_19_103339_add_role_to_users', 2),
(5, '2018_07_19_103631_add_fk_table_users', 2),
(6, '2018_07_19_110412_add_fk_users', 3);

-- --------------------------------------------------------

--
-- Структура таблицы `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('romi4_97@ukr.net', '$2y$10$oERs9QNR.Je65DV7csi3au/y.4HftUypYktl1DDahqQ8Cu5djTkhy', '2018-07-19 10:08:07');

-- --------------------------------------------------------

--
-- Структура таблицы `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `role` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `roles`
--

INSERT INTO `roles` (`id`, `role`) VALUES
(1, 'superAdmin'),
(2, 'admin'),
(3, 'manager'),
(4, 'user');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `role_id` int(11) UNSIGNED NOT NULL DEFAULT '4',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_role_id_foreign` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `name`, `lastName`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `role_id`) VALUES
(3, 'superadmin', 'superadmin', 'superadmin@gmail.com', '$2y$10$.8GQBYj0p9ir3ToPKLmO6OjhQwiziGKxourikKq7FwoaIIlC6R1li', 'tHhvpRPmUEYLmjHowDaUB3fROGCqYqe4QiLCMK2KahnFZA1OQ3sNvOmA3UpU', '2018-07-19 09:34:41', '2018-07-19 09:34:41', 1),
(4, 'admin', 'admin', 'admin@gmail.com', '$2y$10$rH00t7pX2NehS.AGnBiqOOT64KdBZIO4hpyHjYmQLCfoT1q7AYGEy', 'asH0ECn9Y5c8MrAnvX2hIpNceP0k6KcaI2ePKyu9ClmInoyUxgbGgYDgHFaj', '2018-07-19 09:37:36', '2018-07-19 09:37:36', 2),
(5, 'manager', 'manager', 'manager@gmail.com', '$2y$10$PSI.35sOMOPgSlO2aOzlFenObBo69eYgvsIFay0BdHwQ66f15Sj8q', 'IM2Gd8GGgAqmRZq4o7QKXnx6SgZYdE647fTs8lCKm3aERQ7hQ6PntP07sObr', '2018-07-19 09:38:37', '2018-07-19 09:38:37', 3),
(6, 'user', 'user', 'user@gmail.com', '$2y$10$2llcNGsCNIoTKDolXkeDkex8jfIfopWst.DR9f.uE.srm59PUFV6m', 'tVwCPLfF9YLjmyVAjLPwcv35WY8fmqkLiQcitF1JLzULSKXl44zMRjeBrW4q', '2018-07-19 09:39:22', '2018-07-19 10:13:40', 4),
(11, 'qqq', 'qqq', 'romi4_97@ukr.net', '$2y$10$/LVR.E9jPHpStRaLAHvzdOIBgwT65RCc8upl9l6LZmL02LMN6vXdi', '7sGr0M2T4lpkzNUeMME9WOqP4XMNj0AHA0t7pELlqY2PGT6pNZckPp8SqkAn', '2018-07-19 10:07:54', '2018-07-19 10:07:54', 4);

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
